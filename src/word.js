var words = [];

$.ajax({
    type: 'GET',
    url: 'http://localhost/keywords.json'
}).then(res => {
    words = res["داستان کوتاه"]['two_words']
    words = words.map((word, i) => {
        return {value: word, id: make_id("word", String(i + 1000))}
    })
}).then(() => {
    $('#word-numbers').html(words.length)
    $('#word').focus()
    show_words()
}).catch(err => {
    console.log(err);
})


$(document).ready(function() {
    $('#word-numbers').html(words.length)
    $('#word').focus()
    show_words()
})

$('#words-form').on('submit', function(e) {
    e.preventDefault();
    let word = $('#word').val()
    let word_id = make_id("word")
    let already_exists = !! words.filter((value, index, arr) => {
        return value.value == word
    }).length
    if (already_exists) {
        pop_alert({message: "کلمه تکراری", type: "danger", time: 2})
        return false
    }
    words.push({value: word, id: word_id})
    $('#word').val("")
    $('#word-numbers').html(words.length)
    $("#words-box").append(make_element(word, word_id))
})


$('#copy-json').on('click', function(e) {
    e.preventDefault()
    if (words.length < 1) return false
    let jsonArray = []
    words.map(word => {
        jsonArray.push(word.value)
    })
    copyToClipboard(JSON.stringify(jsonArray))
})

$('#copy-text').on('click', function(e) {
    e.preventDefault()
    if (words.length < 1) return false
    let wordsText = "";
    let last_index = words.length - 1
    words.map((word, i) => {
        if (i < last_index) {
            wordsText += `${word.value}\n`
        } else {
            wordsText += `${word.value}`
        }
    })
    copyToClipboard(wordsText)
})

$(document).on('click', '.item-deleter', function() {
    let word_id = $(this).attr('id')
    let new_words = words.filter((value, index, arr) => {
        return value.id != word_id
    })
    words = new_words
    $('#word-numbers').html(words.length)
    show_words()
})

/**
 * This function renders all words as elemnts in DOM
 */
function show_words() {
    $('#words-box').html("")
    words.map((word, i) => {
        $('#words-box').append(make_element(word.value, word.id))
    })
}

/**
 * This function makes a string id for word element
 * @param {string} prefix 
 * @param {string} suffix 
 * @returns 
 */
function make_id(prefix = "", suffix = "") {
    return `${prefix}-${new Date().valueOf()}-${Math.floor(Math.random() * 1000)}${suffix ? suffix : ""}`
}

/**
 * This function makes the word element
 * @param {string} text
 * @param {string} id
 * @returns Javascript Dom Node
 */
function make_element(text, id) {
    let deleter_item = document.createElement('button');
    deleter_item.classList = "badge badge-danger item-deleter"
    deleter_item.innerText = "×"
    deleter_item.id = id
    let wordElement = document.createElement('p')
    wordElement.classList = "word-item"
    wordElement.innerText =  " "+ text
    wordElement.prepend(deleter_item)
    return wordElement
}

/**
 * 
 * @param {object} config
 */
function pop_alert({message, type = "info", time}) {
    let alertive_id = make_id("alertive")
    let alertive = `<p class="alert alert-${type} col-md-6 col-12" id="${alertive_id}">${message}</p>`;
    $("#main-page").prepend(alertive)
    window.setTimeout(() => {
        $(`#${alertive_id}`).fadeOut(500)
    }, time * 1000)
    window.setTimeout(() => {
        $(`#${alertive_id}`).remove()
    }, (time + 2) * 1000)
}

function copyToClipboard(text) {
    navigator.permissions.query({name: "clipboard-write"}).then(result => {
        if (result.state == "granted" || result.state == "prompt") {
            navigator.clipboard.writeText(text).then(function() {
                pop_alert({message: "کپی شد", time: 3})
            }, function() {
                pop_alert({message: "Error", type: "danger", time: 3})
            });
        }
    });
}