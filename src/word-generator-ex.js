const fs = require('fs');
const main_words = ["داستان", "رمان", "داستان کوتاه"]
const prefixes = require('../prefix.json')
const suffixes = require('../suffix.json')
const genres = require('../generes.json')
let final_json = {}
for (let i = 1; i <= 4; i++) {
    eval('var ' + "_" + i + "words" + '= ' + "[]") 
}

main_words.map(word => {
    prefixes.map((prefix, prefixIndex) => {
        _2words.push(`${prefix} ${word} ها`)
        genres.map((gener, generIndex) => {
            _3words.push(`${prefix} ${word} ${gener}`)
            prefixIndex === 0 ? _2words.push(`${word} ${gener}`) : null
            suffixes.adjective.map((adjectiveSuffix, adjectiveSuffixIndex) => {
                prefixIndex === 0 ? _3words.push(`${word} های ${adjectiveSuffix} ${gener}`) : null
                prefixIndex === 0 && generIndex === 0 ? _2words.push(`${word} های ${adjectiveSuffix}`) : null
                suffixes.nationality.map((nationalitySuffix, nationalitySuffixIndex) => {
                    adjectiveSuffixIndex === 0 ? _4words.push(`${prefix} ${word} ${gener} ${nationalitySuffix}`) : null
                    prefixIndex === 0 ? _4words.push(`${word} ${adjectiveSuffix} ${gener} ${nationalitySuffix}`): null
                    generIndex === 0 && adjectiveSuffixIndex === 0  ? _3words.push(`${prefix} ${word} های ${nationalitySuffix}`) : null
                    prefixIndex === 0 && generIndex === 0 ? _3words.push(`${word} های ${adjectiveSuffix} ${nationalitySuffix}`) : null
                    prefixIndex === 0 && adjectiveSuffixIndex === 0 ? _3words.push(`${word} های ${nationalitySuffix} ${gener}`) : null
                    prefixIndex === 0 && generIndex === 0 && adjectiveSuffixIndex === 0 ? _2words.push(`${word} های ${nationalitySuffix}`) : null
                })
            })
        })
    })
    final_json[word] = {
        four_words: _4words,
        three_words: _3words,
        two_words: _2words,
    }
    for (let i = 1; i <= 4; i++) {
        eval("_" + i + "words" + '= ' + "[]") 
    }
})

final_json = JSON.stringify(final_json)
fs.writeFileSync('keywords.json', final_json)