const fs = require('fs')
const key_of = 'داستان کوتاه'
var selected = ["بهترین داستان کوتاه ها","داستان کوتاه عاشقانه","داستان کوتاه های جدید","داستان کوتاه های ایرانی","داستان کوتاه های خارجی","داستان کوتاه های فارسی","داستان کوتاه های خوب","داستان کوتاه های جذاب","داستان کوتاه های معروف","داستان کوتاه ترسناک","داستان کوتاه تخیلی","داستان کوتاه مهاجرت","داستان کوتاه کودک","داستان کوتاه نوجوان","داستان کوتاه طنز","داستان کوتاه آموزنده"]

var three_words = require('./keywords.json')[key_of]['three_words']
var four_words = require('./keywords.json')[key_of]['four_words']

let new_three_words = [];

three_words.map((word, i) => {  
    let includes = selected.filter(x => word.indexOf(x) > 0)
    if (includes.length) {
        new_three_words.push(word)
    }
})

console.log(`three words : ${new_three_words.length}/${three_words.length}`);
fs.writeFileSync(`../omit-results/${key_of}-three_words.json`,  JSON.stringify(new_three_words))

let new_four_words = [];

four_words.map((word, i) => {
    let includes = new_three_words.filter(x => {
        return word.indexOf(x) > -1
    })
    if (includes.length) {
        new_four_words.push(word)
    }
})

console.log(`four words : ${new_four_words.length}/${four_words.length}`);
fs.writeFileSync(`../omit-results/${key_of}-four_words.json`, JSON.stringify(new_four_words))
